# js-create_form

create form

```javascript
import React from "react";
import axios from "axios";

class Form extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			values: {
				email: "",
				password: ""
			},
			errors: {
				email: [],
				password: []
			}
		};

		this.form = createForm(
			{
				email: ({ onChange }) => ({
					props: {
						type: "text",
						value: state.values.email,
						onChange: e => onChange(e.target.value)
					}
				}),
				password: ({ onChange }) => ({
					props: {
						type: "password",
						value: state.values.email,
						onChange: e => onChange(e.target.value)
					}
				})
			},
			{
				get: () => ({ ...this.state.values }),
				set: (errors, values) => {
					this.setState({
						errors: errors,
						values: values
					});
				},
				changeset: changeset =>
					changeset
						.validateRequired(["email", "password"])
						.validateFormat("email", /@/)
						.validateLength("password", { ">=": 6, "<=": 1024 })
			}
		);

		this.onSubmit = e => {
			e.preventDefault();

			if (this.form.isValid()) {
				const { email, password } = this.state.values;

				axios
					.post("/sign_in", { email, password })
					.then(response => {
						console.log(response);
						this.form.reset();
					})
					.catch(errors => this.form.setErrors(errors));
			}
		};
	}

	render() {
		const { errors, values } = this.state,
			fields = this.form.render(errors, values);

		return (
			<form onSubmit={this.onSubmit}>
				<input {...fields.email.props} />
				<Errors errors={fields.email.errors} />
				<br />
				<input {...fields.password.props} />
				<Errors errors={fields.email.errors} />
				<br />
				<input type="submit" value="Submit" onClick={this.onSubmit} />
			</form>
		);
	}
}

const Errors = ({ errors }) => (
	<ul>
		{errors.map((error, index) => <li key={index}>{error.message}</li>)}
	</ul>
);
```
