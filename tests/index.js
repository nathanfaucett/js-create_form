var tape = require("tape"),
	Changeset = require("@nathanfaucett/changeset"),
	extend = require("@nathanfaucett/extend"),
	createForm = require("..");

tape("createForm", function(assert) {
	var state = {
		values: {
			email: "",
			password: ""
		},
		errors: {
			email: [],
			password: []
		}
	};

	var form = createForm(
		{
			email: function(field) {
				return {
					props: {
						value: state.values.email,
						onChange: function(e) {
							field.onChange(e.target.value);
						}
					}
				};
			},
			password: function(field) {
				return {
					props: {
						value: state.values.password,
						onChange: function(e) {
							field.onChange(e.target.value);
						}
					}
				};
			}
		},
		{
			get: function() {
				return extend({}, state.values);
			},
			set: function(errors, values) {
				state.errors = errors;
				state.values = values;
			},
			changeset: function changeset(changeset) {
				return changeset
					.validateRequired(["email", "password"])
					.validateFormat("email", /@/)
					.validateLength("password", { ">=": 6, "<=": 1024 });
			}
		}
	);

	var rendered = form.render(state.errors, state.values);

	assert.deepEquals(rendered.email.errors, []);
	assert.equals(rendered.email.props.value, "");

	assert.deepEquals(rendered.password.errors, []);
	assert.equals(rendered.password.props.value, "");

	form.fields.email.props.onChange({ target: { value: "bob" } });
	form.fields.password.props.onChange({ target: { value: "pass" } });

	var rendered = form.render(state.errors, state.values);

	assert.deepEquals(rendered.email.errors, [
		{ message: "format", keys: [/@/] }
	]);
	assert.deepEquals(rendered.password.errors, [
		{ message: "length", keys: ["gte", 6] }
	]);

	form.fields.email.props.onChange({ target: { value: "bob@gmail.com" } });
	form.fields.password.props.onChange({ target: { value: "password" } });

	var rendered = form.render(state.errors, state.values);

	assert.deepEquals(rendered.email.errors, []);
	assert.equals(rendered.email.props.value, "bob@gmail.com");

	assert.deepEquals(rendered.password.errors, []);
	assert.equals(rendered.password.props.value, "password");

	form.setErrors({
		email: [{ message: "invalid_domain" }],
		password: ["missing_required_characters"]
	});

	var rendered = form.render(state.errors, state.values);

	assert.deepEquals(rendered.email.errors, [
		{ message: "invalid_domain", keys: [] }
	]);
	assert.deepEquals(rendered.password.errors, [
		{ message: "missing_required_characters", keys: [] }
	]);

	assert.end();
});
