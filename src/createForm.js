var keys = require("@nathanfaucett/keys"),
	has = require("@nathanfaucett/has"),
	isFunction = require("@nathanfaucett/is_function"),
	arrayForEach = require("@nathanfaucett/array-for_each"),
	objectForEach = require("@nathanfaucett/object-for_each"),
	extend = require("@nathanfaucett/extend"),
	isEmpty = require("@nathanfaucett/is_empty"),
	Changeset = require("@nathanfaucett/changeset");

module.exports = createForm;

function createForm(fields, options) {
	var form = {
			fields: {}
		},
		initalState = {},
		modified = {},
		fieldNames,
		changeset,
		changesetFn,
		get,
		set;

	fields = fields || {};
	options = options || {};

	get = ensureIsFunction("get", options.get);
	set = ensureIsFunction("set", options.set);

	changesetFn = isFunction(options.changeset)
		? options.changeset
		: defaultChangesetFn;

	function onUpdate(name, value) {
		var values = get(),
			filteredErrors = {},
			errors;

		modified[name] = true;
		values[name] = value;

		changeset = changesetFn(changeset.putChanges(values).clearErrors());

		errors = changeset.getErrors();
		values = changeset.getChanges();

		objectForEach(errors, function each(errors, key) {
			if (key === name || modified[key]) {
				filteredErrors[key] = errors;
			} else {
				filteredErrors[key] = [];
			}
		});

		set(filteredErrors, values);
	}

	objectForEach(fields, function each(createFieldFn, name) {
		var field = (form.fields[name] = createField(
			name,
			createFieldFn,
			onUpdate
		));
		initalState[name] = field.props.value || "";
	});

	fieldNames = keys(form.fields);
	changeset = new Changeset(initalState, fieldNames);

	form.isValid = function() {
		changeset = changesetFn(changeset.putChanges(get()).clearErrors());

		if (changeset.isValid()) {
			return true;
		} else {
			set(changeset.getErrors(), changeset.getChanges());
			return false;
		}
	};

	form.reset = function() {
		modified = {};
		changeset = new Changeset(initalState, fieldNames);
		set(changeset.getErrors(), changeset.getChanges());
	};

	form.setErrors = function(errors) {
		changeset.clearErrors();

		objectForEach(errors, function eachErrors(errors, name) {
			arrayForEach(errors, function eachError(error) {
				var message = error.message ? error.message : error,
					keys = error.keys ? error.keys : [];

				changeset.addError(name, message, keys);
			});
		});

		set(changeset.getErrors(), get());
	};

	form.render = function(errors, values) {
		var fields = {};

		objectForEach(form.fields, function each(field, name) {
			var fieldErrors = errors[name] || [];

			fields[name] = {
				errors: fieldErrors,
				props: extend(
					{},
					field.props,
					field.render(field, fieldErrors, values[name])
				)
			};
		});

		return fields;
	};

	return form;
}

function createField(name, createFieldFn, onUpdate) {
	return extend(
		{
			render: function(field, errors, value) {
				return {
					value: value || ""
				};
			}
		},
		createFieldFn({
			name: name,
			onChange: function onChange(value) {
				return onUpdate(name, value);
			}
		})
	);
}

function ensureIsFunction(key, value) {
	if (isFunction(value)) {
		return value;
	} else {
		throw new TypeError(
			"createForm(fields: {}, options: {}) options." +
				key +
				" must be a Function"
		);
	}
}

function defaultChangesetFn(changeset) {
	return changeset;
}
